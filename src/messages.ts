/*

CREATES AND CONTROLS MESSAGES

*/

export class Message {

    // MESSAGE FIELDS
    public type: string;
    public origin: string;
    public sendId: string;
    public timestamp: string;
    public responseId: string;
    public hasResponse: boolean;
    public destination: string;

    public data: object = {};

    constructor() {
        //
    }

    public toString(): string {
        return JSON.stringify(this.toJSON());
    }

    public toJSON(): object {
        let msg = {
            data: this.data,
            type: this.type,
            origin: this.origin,
            sendId: this.sendId,
            timestamp: this.timestamp,
            responseId: this.responseId,
            hasResponse: this.hasResponse,
            destination: this.destination
        }

        return (msg);
    }

    //

    public setType(type: string): Message {
        this.type = type;

        return this;
    }

    public setOrigin(origin: string): Message {
        this.origin = origin;

        return this;
    }

    public setData(data: object): Message {
        this.data = data;

        return this;
    }

    public setSendId(sendId: string): Message {
        this.sendId = sendId;

        return this;
    }

    public setTimestamp(): Message {
        let timestamp = (new Date()).toString();
        this.setTimestamp_(timestamp);

        return this;
    }

    public setTimestamp_(timestamp: string): Message {
        this.timestamp = timestamp;

        return this;
    }

    public setResponseId(responseId: string): Message {
        this.responseId = responseId;

        return this;
    }

    public setHasResponse(hasResponse: boolean): Message {
        this.hasResponse = hasResponse;

        return this;
    }

    public setDestination(destination: string): Message {
        this.destination = destination;

        return this;
    }

    ////////////////////////////////////////////////////////////

    public static Parse(str: string): Message {
        let m = new Message();
        let j = JSON.parse(str);

        m.setData(j['data'])
            .setType(j['type'])
            .setOrigin(j['origin'])
            .setSendId(j['sendId'])
            .setTimestamp_(j['timestamp'])
            .setResponseId(j['responseId'])
            .setHasResponse(j['hasResponse'])
            .setDestination(j['destination']);

        return m;
    }
}