import { Message } from '../messages';
import generateHash from 'random-hash';

import FlakeId = require('flake-idgen');

export abstract class AbstractMessageBuilder {
    protected message: Message;
    protected data: object = {};

    protected msgGeneratorSeedId;
    protected messageIDgenerator

    protected senderNodeHandle: string;

    constructor(senderNodeHandle: string){
        this.senderNodeHandle = senderNodeHandle;
        this.msgGeneratorSeedId = generateHash({
            length: 10,
            charset: [0, 1]
        });

        this.messageIDgenerator = new FlakeId({
            id: this.msgGeneratorSeedId
        });

        this.message = new Message();
        this.message.setOrigin(senderNodeHandle)
                    .setSendId(this.messageIDgenerator.next())
                    .setTimestamp();

    }

    public returnMessage(): Message {
        this.setData();

        return this.message;
    }

    public returnMessageString(): string {
        return this.message.toString();
    }

    protected setData(): AbstractMessageBuilder {
        this.message.setData(this.data);

        return this;
    }

}