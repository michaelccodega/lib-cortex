import { AbstractMessageBuilder } from './abstract.message.builder';
import { Message }  from '../messages';

export class PublishMessageBuilder extends AbstractMessageBuilder {

    // Constructor
    constructor(senderNodeHandle: string){
        super(senderNodeHandle);

        this.message.setType('publish');
    }

    //
    public setEvent(eventHandle: string): PublishMessageBuilder {
        this.data['event'] = eventHandle;
        this.setData();

        return this;
    }

    public setMessage(message: string): PublishMessageBuilder {
        this.data['message'] = message;
        this.setData();

        return this;
    }

    
}