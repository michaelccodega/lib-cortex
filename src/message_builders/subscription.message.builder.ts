import { AbstractMessageBuilder } from './abstract.message.builder';
import { Message }  from '../messages';

export class SubscriptionMessageBuilder extends AbstractMessageBuilder {

    constructor(senderNodeHandle: string){
        super(senderNodeHandle);

        this.message.setType('subscription');
        this.data['unsub'] = false;
        this.data['pub'] = false;
    }

    public setSubscriber(subscriberHandle: string): SubscriptionMessageBuilder {
        this.data['subscriber'] = subscriberHandle;
        this.setData();

        return this;
    }

    public setEvent(eventHandle: string): SubscriptionMessageBuilder {
        this.data['eventHandle'] = eventHandle;
        this.setData();

        return this;
    }

    public setUnsub(): SubscriptionMessageBuilder {
        this.data['unsub'] = true;
        this.setData();

        return this;
    }

    public setPublish(): SubscriptionMessageBuilder {
        this.data['pub'] = true;
        this.setData();

        return this;
    }
}