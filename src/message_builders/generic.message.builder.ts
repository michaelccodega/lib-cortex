import { AbstractMessageBuilder } from './abstract.message.builder';
import { Message }  from '../messages';

export class GenericMessageBuilder extends AbstractMessageBuilder {

    public setGenericData(data: object): GenericMessageBuilder {
        this.data = data;
        this.setData();

        return this;
    }
}