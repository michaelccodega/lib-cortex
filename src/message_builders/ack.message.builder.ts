import { AbstractMessageBuilder } from './abstract.message.builder';
import { Message }  from '../messages';

export class AckMessageBuilder extends AbstractMessageBuilder {

    constructor(senderNodeHandle: string){
        super(senderNodeHandle);

        this.message.setType('ack');
        this.data = {
            type: 'ack'
        };
    }

    public setAcknowledgeRegister(): AckMessageBuilder { 
        this.data['type'] = 'ack_reg';
        this.setData();

        return this;
    }

    public setAckMessage(ackMessage: string): AckMessageBuilder {
        this.data['ackMessage'] = ackMessage;
        this.setData();

        return this;
    }

}