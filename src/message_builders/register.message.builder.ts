import { AbstractMessageBuilder } from './abstract.message.builder';

export class RegisterMessageBuilder extends AbstractMessageBuilder {

    protected data: object = {};

    //constructor
    constructor(senderNodeHandle: string){
        super(senderNodeHandle);

        this.message.setType('register')
                    .setHasResponse(true);

        this.data['nodeHandle'] = senderNodeHandle;
        this.data['uuid'] = '';
        this.data['parentCoreId'] = '';
    }

    /////////// PUBLIC BUILDER FUNCTIONS

    public setUUID(uuid: string): RegisterMessageBuilder {
        this.data['uuid'] = uuid;
        this.setData();

        return this;
    }

    public setParentCoreId(parentCoreId: string): RegisterMessageBuilder {
        this.data['parentCoreId'] = parentCoreId;
        this.setData();

        return this;
    }

    /////////// PRIVATE FUNCTIONS

    protected setData(): RegisterMessageBuilder {
        this.message.setData(this.data);

        return this;
    }
}