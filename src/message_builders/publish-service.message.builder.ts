import { AbstractMessageBuilder } from './abstract.message.builder';
import { Message }  from '../messages';

export class PublishServiceMessageBuilder extends AbstractMessageBuilder {

    constructor(senderNodeHandle: string){
        super(senderNodeHandle);

        this.message.setType('register-service');
        this.data['origin'] = this.message.origin;
    }

    public setServiceHandle(serviceHandle: string): PublishServiceMessageBuilder {
        this.data['serviceHandle'] = serviceHandle;
        this.setData();

        return this;
    }

    public setParentNode(parentNode: string): PublishServiceMessageBuilder {
        this.data['parentNode'] = parentNode;
        this.setData();

        return this;
    }

    public setServiceParams(params: object): PublishServiceMessageBuilder {
        this.data['params'] = params;
        this.setData();

        return this;
    }
}