import { AbstractMessageBuilder } from './abstract.message.builder';
import { Message }  from '../messages';

export class WarnMessageBuilder extends AbstractMessageBuilder {

    constructor(senderNodeHandle: string){
        super(senderNodeHandle);

        this.message.setType('warn');
        this.data = {
            type: 'warn',
            warning: 'unknown'
        };

        this.setData();
    }

    public warning(warning: string): WarnMessageBuilder {
        this.data['warning'] = warning;
        this.setData();

        return this;
    }
}