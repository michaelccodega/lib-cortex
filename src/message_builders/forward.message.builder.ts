//
import { AbstractMessageBuilder } from './abstract.message.builder';
import { Message }  from '../messages';

export class ForwardMessageBuilder extends AbstractMessageBuilder {

    public setMessage(message: string): ForwardMessageBuilder {
        this.data = JSON.parse(message);
        this.setData();

        return this;
    }
}