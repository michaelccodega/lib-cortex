///
import * as nano from 'nanomsg';
import * as fs from 'fs';

/**
 * This is a collection of constants
 */
export class Const { 
    public static SOCKET_DELAY: number = 50;
}