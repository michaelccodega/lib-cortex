//

export class Log {

    public logs: boolean = true;
    public nodeHandle: string = '';
    constructor(nodeHandle){
        //
        this.nodeHandle = nodeHandle;
    }

    setLogging(logs: boolean): void {
        this.logs = logs;
    }
    
    public this(message: string): void {
        if(this.logs){
            this.log( '[' + this.nodeHandle + '] ' + message + (new Date()).toDateString());
        }
    }

    public test(message: string): void {
        if(this.logs){
            this.log('[TEST]' + message + (new Date()).toDateString());
        }
    }

    private log(message: string): void {
        console.log(message);
    }
}