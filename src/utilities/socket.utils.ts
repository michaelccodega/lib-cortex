///
import * as nano from 'nanomsg';
import * as fs from 'fs';

import { Const } from './const.utils';

export class SocketUtils {
    /**
     * Creates a IPC socket that listens for an event, return object with 2 params: {socket, address}
     * @param eventHandle string of the event to listen to
     * @param eventHandler takes 2 arguments: eventHandler(eventHandle, message);
     */
    public static newEventBus(eventHandle: string, eventHandler: Function): object {

        let socket: nano.Socket = nano.socket('bus');
        let address: string = '/tmp/jones_core_event_' + eventHandle + '_.ipc';
        socket.connect(SocketUtils.pathToSocketAddress(address));
        socket.on('data', (message) => {
            eventHandler(eventHandle, message);
        });

        return {
            socket: socket,
            address: address
        }
    }

    /**
     * 
     * @param socketType 
     */
    public static createSocket(socketType: string): nano.Socket {
        let socket: nano.Socket = nano.socket(socketType);

        return socket;
    }

    /**
     * 
     * @param socketType 
     * @param address 
     */
    public static createSocketMaster(socketType: string, address: string): nano.Socket {
        let socket: nano.Socket = SocketUtils.createSocket(socketType);
        SocketUtils.freeIPC(address);
        socket.bind(SocketUtils.pathToSocketAddress(address));

        return socket;
    }

    /**
     * 
     * @param socketType 
     * @param address 
     */
    public static createSocketSlave(socketType: string, address: string): nano.Socket {
        let socket: nano.Socket = SocketUtils.createSocket(socketType);
        socket.connect(SocketUtils.pathToSocketAddress(address));

        return socket;
    }

    /**
     * This clears the IPC address if it is currently in use. It does this by deleting the FileSystem object that represents the IPC socket
     * @param ipcAddress 
     */
    public static freeIPC(ipcAddress: string): void {
        if (fs.existsSync(ipcAddress)) {
            fs.unlinkSync(ipcAddress);
        }
    }

    /**
     * This is used to automate the sending data across a socket.  The socket connection requires a time delay, and adding that makes code ugly so this hides that. Note that this also closes the socket after, and connects on beginning
     * @param socket 
     * @param request 
     * @param address 
     */
    public static sendOnSocket(socket: nano.Socket, request: string, address: string): void {
        //console.log(address);
        socket.connect(address);

        setTimeout(() => {
            socket.send(request);
            setTimeout(() => {
                socket.shutdown(address);
            }, Const.SOCKET_DELAY);
        }, Const.SOCKET_DELAY);
    }


    /**
     * This clears the IPC address if it is currently in use. It does this by deleting the FileSystem object that represents the IPC socket
     * @param ipcAddress 
     * @param path 
     */
    public static freeIPCfile(ipcAddress: string, path: string): void {
        try {
            if (fs.existsSync(path + ipcAddress)) {
                fs.unlinkSync(path + ipcAddress);
            } else {
                if (fs.existsSync(ipcAddress)) {
                    fs.unlinkSync(ipcAddress);
                }
            }
        } catch (error) {
            console.log(error);
        }
    }

    /**
     * 
     * @param address 
     */
    public static pathToSocketAddress(address: string): string {
        //this could be extended to handle things like a file path, so TMP isnt the location anymore
        return 'ipc://' + address;
    }
}