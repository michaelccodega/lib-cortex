/**
 * 
 */
export class IPCUtils {

    public static encodeGeneric(str: string): string {
        return(encodeURIComponent(str));
    }
    
    /**
     * 
     * @param serviceHandle 
     */
    public static encodeAddressService(serviceHandle: string): string {
        let address = '/tmp/jones_service_' + encodeURIComponent(serviceHandle) + '_.ipc';
        //console.log(address);
        return address;
    }

    /**
     * 
     * @param eventHandle 
     */
    public static encodeAddressEvent(eventHandle: string): string {
        return '/tmp/jones_core_event_' + encodeURIComponent(eventHandle) + '_.ipc';
    }

    /**
     * 
     * @param eventHandle 
     */
    public static encodeAddressNode(nodeHandle: string): string {
        return '/tmp/jones_core_node_' + encodeURIComponent(nodeHandle) + '_.ipc';
    }
}