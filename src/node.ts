////////// IMPORTS
//
import { Const } from './utils';
import { SocketUtils } from './utils';
import { IPCUtils } from './utils'

import { Message } from './messages';

import { RegisterMessageBuilder } from './builder';
import { SubscriptionMessageBuilder } from './builder';
import { AckMessageBuilder } from './builder';
import { PublishServiceMessageBuilder } from './builder';

// Importing since this is our transport protocol
import * as nano from 'nanomsg';
// This is used to clear old IPC events 
import * as fs from 'fs';
import { PublishMessageBuilder } from './message_builders/publish.message.builder';

// This is a self contained module that can communicate with the Cortex Core
// it is self contained, and can be used in a Cortex node as a drop in
//
export class Node {

    //////// FIELDS FOR NODE CORE IDENTIFICATION AND INITIALIZATION
    // This is the identifying string for the node (should be unique)
    public handle: string;
    // This is the uuid for the node (will be unique) //todo: maybe (or only for cores?)
    public uuid: string;
    // This is the configuration data
    private config: object;
    // This is if the node is registered (or needs a heartbeat update)
    private isRegistered: boolean;
    // This is the id used in generating message ID's
    private msgGeneratorSeedId: number;
    // This is the stateful generator used to create message Id's (for traacking responses)
    private messageIDgenerator;

    //////// FIELDS FOR THE IPC INTERFACE
    // This is the IPC address used to find the JonesCore
    private ipcAddress: string;
    // This is the socket used to interface with the JonesCore IPC channel 
    private ipcSocket: nano.Socket;

    //////// FIELDS FOR THE IPC SUBSCRIPTION FUNCTIONALITY
    // This stores the list of subscriptions and sockets that listen on those IPC channels
    private subscriptions: { [key: string]: any } = {};
    private subscriptionSocket;
    private subscriptionAddresses: Array<string> = [];

    //////// FIELDS FOR THE IPC SERVICE FUNCTIONALITY
    // This stores a list of services that are offered by the node
    private services: { [key: string]: any } = {};
    private nodeServiceAddress: string;
    private nodeServiceSocket: nano.Socket;
    private serviceSocket: nano.Socket;

    //////// FIELDS FOR DEBUGGING
    public logging: boolean = false;
    public test: boolean = false;
    //////// CONSTRUCTOR
    /**
     * Constructor for a Cortex Node
     * @param nodeHandle this is a unique string that identifies the node
     * @param config this is a JSON oject that contains parameters for construction of node
     */
    constructor(nodeHandle: string, config: object) {
        //set up config
        this.handle = nodeHandle;
        this.configure(config);
        //rm:
        //this.logging = true;

        //set up the IPC interface
        this.ipcSocket = SocketUtils.createSocketSlave('req', this.ipcAddress);
        this.ipcSocket.on('data', (data: Buffer) => {
            this.handleIPCdata(data.toString());
        });

        //register the node
        this.isRegistered = false;
        this.registerNode();
        this.subscribe('system', (topic: string, message: string) => {
            if (this.logging) {
                console.log('[' + this.handle + '] new message on topic: ' + topic);
            }

            this.handleSystemMessage(message);
        });

        // Create a socket to listen on for the "NodeService" (implicit service with node handle as name)

        this.nodeServiceAddress = IPCUtils.encodeAddressNode(this.handle);
        this.nodeServiceSocket = SocketUtils.createSocketMaster('rep', this.nodeServiceAddress);
        this.nodeServiceSocket.on('data', (msg: Buffer) => {
            this.nodeServiceSocket.send(this.handleNodeServiceMessage(msg.toJSON()));
        });
    }

    ///////////// public methods
    /**
     * 
     */
    public spin(): void {
        // this should ensure that the thing runs... it can spawn stuff or something
        // in the future this can spawn a thread or fiber to perform maintenance or long-running processes
    }

    /**
     * Subscribe to an event, triggers callback. If callback is `undefined` it unsubscribes
     * @param eventHandle 
     * @param callback 
     */
    public subscribe(eventHandle: string, callback: Function): void {
        if (this.logging) {
            console.log('[Node] Registered Subscription: ' + eventHandle);
        }

        if (callback == undefined) {
            //unsubscribe
            this.subscriptions[eventHandle] = undefined;
            this.sendMessage((new SubscriptionMessageBuilder(this.handle))
                .setEvent(eventHandle)
                .setSubscriber(this.handle)
                .setUnsub()
                .returnMessage()
            );

            if (this.logging) {
                console.log('[Node] Unregistered Subscription: ' + eventHandle);
            }

            return;
        } else {

            this.sendMessage((new SubscriptionMessageBuilder(this.handle))
                .setEvent(eventHandle)
                .setSubscriber(this.handle)
                .returnMessage()
            );

            let address: string = IPCUtils.encodeAddressEvent(eventHandle);
            let socket: nano.Socket = SocketUtils.createSocketSlave('bus', address);

            if (this.logging) {
                if (this.test) {
                    console.log('[test] listening to channel: ' + address);
                }
            }

            socket.on('data', (data: Buffer) => {
                if (this.logging) {
                    console.log('data! : ');
                }
                console.log(data.toString());
                callback(data.toString());
            });

            this.subscriptions[eventHandle] = {
                eventHandle: eventHandle,
                address: address,
                socket: socket
            };

            return;
        }
    }

    /**
     * 
     * @param eventHandle 
     * @param data 
     */
    public publish(eventHandle: string, data: string): void {
        eventHandle = IPCUtils.encodeGeneric(eventHandle);

        if (this.logging) {
            console.log('[Node] publishing to event ' + eventHandle);
        }

        let message: Message = new PublishMessageBuilder(this.handle)
            .setEvent(eventHandle)
            .setMessage(data)
            .returnMessage();

        this.sendMessage(message);
    }

    /**
     * 
     * @param nodeHandle 
     * @param request 
     * @param callback 
     */
    public makeRequest(serviceHandle: string, request: string, callback: Function): void {
        //
        /*

        NOW THIS WILL DO SOMETHING NEW ****

        This will now send the message to the core, who will send it to the correct 
        service (registered service*) and use the sendMessage() function to send it to that
        node. It will go to the specific "in" channel for that node, so its known to the node
        what service it is for (maybe just the node's main address??).  The node gets the 
        "origin" address and uses that to reply - and the chain cycles back.

        The node will need a "message waiting" table for messages that need a reply. Whenever
        it adds a message, it should add a timer. That timer will be for 10 seconds. After
        10 seconds, it checks the message waiting table. If that message is still there, it 
        removes the entry.  

        When the Node recieves a message that isnt a predefines one, it checks the message 
        number with the message waiting LUT. If the messsage is in the lut, there should be 
        a callback function. Else: throw out message.

        */

        let address: string = IPCUtils.encodeAddressService(serviceHandle);
        //console.log(address);
        let socket: nano.Socket = SocketUtils.createSocketSlave('req', address);
        socket.on('data', (response) => {
            //console.log('response: ' + response);
            let res = JSON.parse(response.toString());
            callback(res);
        });

        SocketUtils.sendOnSocket(socket, request, address);
    }

    /**
     * 
     * @param serviceHandle 
     * @param serviceParams 
     * @param callback 
     */
    public publishService(serviceHandle: string, serviceParams: Array<string>, callback: Function): void {
        if (this.logging) {
            console.log('[' + this.handle + '] CREATING NEW SERVICE: ' + serviceHandle);
        }

        let address: string = IPCUtils.encodeAddressService(serviceHandle);
        let socket = SocketUtils.createSocketMaster('rep', address);
        socket.on('data', (data: Buffer) => {
            let message: object = JSON.parse(data.toString());
            socket.send(callback(message));

        });

        this.services[serviceHandle] = {
            serviceHandle: serviceHandle,
            serviceParams: serviceParams,
            handler: callback,

            address: address,
            socket: socket
        };

        this.sendMessage((new PublishServiceMessageBuilder(this.handle))
            .setServiceHandle(serviceHandle)
            .setServiceParams(serviceParams)
            .returnMessage()
        );
    }

    ///////////// PRIVATE METHODS
    // PRIVATE ACTION METHODS 
    /**
     * 
     * @param msg 
     */
    private handleIPCdata(msg: string): void {
        let message = JSON.parse(msg);

        if (!message['type']) {
            if (this.logging) {
                console.log('[Node] recieved message of unknown type');
            }
        } else {
            switch (message['type']) {
                case 'ack': {
                    return this.handleAckMessage(message);
                }
                case 'response': {
                    return this.handleResponseMessage(message);
                }
                case 'warn': {
                    // handle this somehow???
                    return;
                }
                default: {
                    if (this.logging) {
                        console.log('[Node] recieved message of unknown type');
                    }
                }
            }
        }
    }

    // MESSAGE HANDLERS
    // ipc from core:
    /**
     * 
     * @param message 
     */
    private handleAckMessage(message: object): void {
        if (message['data']) {
            if (message['data']['type'] == 'ack_reg') {
                this.isRegistered = true;
            }
        }
    }

    /**
     * 
     * @param message 
     */
    private handleResponseMessage(message: object): void {
        if (this.logging) {
            console.log('got a response: ' + message);
        }

    }

    // Subscriptions:
    /**
     * 
     * @param message 
     */
    private handleSystemMessage(message: string): void {
        if (this.logging) {
            console.log(message);
            //console.log(JSON.stringify(message));
        }
    }

    // Requests:
    /**
     * 
     * @param message 
     */
    private handleNodeServiceMessage(message: object): string {
        if (this.logging) {
            console.log('[' + this.handle + '] new node request made...');
            console.log(JSON.stringify(message));
        }

        return '';
    }

    // UTILITY FUNCTIONS

    /**
     * 
     * @param message 
     */
    protected sendMessage(message: Message): void {
        // this is also a god spot to save the message ID in case of callbacks?
        // make have an optional third return value that allows the callback to 
        // be saved and called on messages that use that message id
        // some sort of 'is a reply' flag?
        if (message.hasResponse) {
            //do something
        }

        if (this.logging) {
            console.log('sending the following message: ' + message.toString() + '\n');
        }
        this.ipcSocket.send(message.toString());
    }

    /**
     * 
     * @param message 
     */
    protected sendString(message: string): void {
        // this is also a god spot to save the message ID in case of callbacks?
        // make have an optional third return value that allows the callback to 
        // be saved and called on messages that use that message id
        // some sort of 'is a reply' flag?
        //if (message.hasResponse) {//do something}
        if (this.logging) {
            console.log(message)
        }
        this.ipcSocket.send(message.toString());
    }

    /**
     * 
     */
    private registerNode(): void {
        // send the registration message repeatedly until it succeeds
        let services: Array<object> = [];
        Object.keys(this.services).forEach(service => {
            services.push({
                serviceHandle: service['serviceHandle'],
                origin: this.handle,
                params: service['params']
            });
        });

        setTimeout(() => {
            this.sendMessage((new RegisterMessageBuilder(this.handle))
                .returnMessage()
            );
        }, Const.SOCKET_DELAY);
    }

    /**
     * 
     * @param config 
     */
    private configure(config: object): void {
        /////// MANIPULATE THE CONFIG OBJECT AS NEEDED 
        //todo accept a version number?
        this.config = config;

        if (this.config['ipc_addr']) {
            this.ipcAddress = this.config['ipc_addr'];
            if (this.logging) {
                console.log('[jones] Setting the IPC Address to: ' + this.ipcAddress);
            }

        } else {
            if (this.config['localTransport']) {
                // if want to override IPC and use TCP for some reason
                if (this.config['localTransport'] == 'tcp') {
                    this.ipcAddress = 'tcp://127.0.0.1:3000';
                }
                if (this.config['localTransport'] == 'ipc') {
                    this.ipcAddress = '/tmp/jones_core.ipc';
                }
            } else {
                // no default method option chosen, so go with IPC and not TCP
                this.ipcAddress = '/tmp/jones_core.ipc';
            }
        }

        if (this.config['logging']) {
            if (this.config['logging'] == true) {
                this.logging = true;
            }
        }

        if (this.config['test']) {
            if (this.config['test'] == true) {
                this.test = true;
            }
        }
    }

}