"use strict";
/*

CREATES AND CONTROLS MESSAGES

*/
Object.defineProperty(exports, "__esModule", { value: true });
var Message = /** @class */ (function () {
    function Message() {
        this.data = {};
        //
    }
    Message.prototype.toString = function () {
        return JSON.stringify(this.toJSON());
    };
    Message.prototype.toJSON = function () {
        var msg = {
            data: this.data,
            type: this.type,
            origin: this.origin,
            sendId: this.sendId,
            timestamp: this.timestamp,
            responseId: this.responseId,
            hasResponse: this.hasResponse,
            destination: this.destination
        };
        return (msg);
    };
    //
    Message.prototype.setType = function (type) {
        this.type = type;
        return this;
    };
    Message.prototype.setOrigin = function (origin) {
        this.origin = origin;
        return this;
    };
    Message.prototype.setData = function (data) {
        this.data = data;
        return this;
    };
    Message.prototype.setSendId = function (sendId) {
        this.sendId = sendId;
        return this;
    };
    Message.prototype.setTimestamp = function () {
        var timestamp = (new Date()).toString();
        this.setTimestamp_(timestamp);
        return this;
    };
    Message.prototype.setTimestamp_ = function (timestamp) {
        this.timestamp = timestamp;
        return this;
    };
    Message.prototype.setResponseId = function (responseId) {
        this.responseId = responseId;
        return this;
    };
    Message.prototype.setHasResponse = function (hasResponse) {
        this.hasResponse = hasResponse;
        return this;
    };
    Message.prototype.setDestination = function (destination) {
        this.destination = destination;
        return this;
    };
    ////////////////////////////////////////////////////////////
    Message.Parse = function (str) {
        var m = new Message();
        var j = JSON.parse(str);
        m.setData(j['data'])
            .setType(j['type'])
            .setOrigin(j['origin'])
            .setSendId(j['sendId'])
            .setTimestamp_(j['timestamp'])
            .setResponseId(j['responseId'])
            .setHasResponse(j['hasResponse'])
            .setDestination(j['destination']);
        return m;
    };
    return Message;
}());
exports.Message = Message;
//# sourceMappingURL=messages.js.map