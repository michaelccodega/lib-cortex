"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This is a collection of constants
 */
var Const = /** @class */ (function () {
    function Const() {
    }
    Const.SOCKET_DELAY = 50;
    return Const;
}());
exports.Const = Const;
//# sourceMappingURL=const.utils.js.map