export declare class Log {
    logs: boolean;
    nodeHandle: string;
    constructor(nodeHandle: any);
    setLogging(logs: boolean): void;
    this(message: string): void;
    test(message: string): void;
    private log(message);
}
