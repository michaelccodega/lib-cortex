import * as nano from 'nanomsg';
export declare class SocketUtils {
    /**
     * Creates a IPC socket that listens for an event, return object with 2 params: {socket, address}
     * @param eventHandle string of the event to listen to
     * @param eventHandler takes 2 arguments: eventHandler(eventHandle, message);
     */
    static newEventBus(eventHandle: string, eventHandler: Function): object;
    /**
     *
     * @param socketType
     */
    static createSocket(socketType: string): nano.Socket;
    /**
     *
     * @param socketType
     * @param address
     */
    static createSocketMaster(socketType: string, address: string): nano.Socket;
    /**
     *
     * @param socketType
     * @param address
     */
    static createSocketSlave(socketType: string, address: string): nano.Socket;
    /**
     * This clears the IPC address if it is currently in use. It does this by deleting the FileSystem object that represents the IPC socket
     * @param ipcAddress
     */
    static freeIPC(ipcAddress: string): void;
    /**
     * This is used to automate the sending data across a socket.  The socket connection requires a time delay, and adding that makes code ugly so this hides that. Note that this also closes the socket after, and connects on beginning
     * @param socket
     * @param request
     * @param address
     */
    static sendOnSocket(socket: nano.Socket, request: string, address: string): void;
    /**
     * This clears the IPC address if it is currently in use. It does this by deleting the FileSystem object that represents the IPC socket
     * @param ipcAddress
     * @param path
     */
    static freeIPCfile(ipcAddress: string, path: string): void;
    /**
     *
     * @param address
     */
    static pathToSocketAddress(address: string): string;
}
