"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
///
var nano = require("nanomsg");
var fs = require("fs");
var const_utils_1 = require("./const.utils");
var SocketUtils = /** @class */ (function () {
    function SocketUtils() {
    }
    /**
     * Creates a IPC socket that listens for an event, return object with 2 params: {socket, address}
     * @param eventHandle string of the event to listen to
     * @param eventHandler takes 2 arguments: eventHandler(eventHandle, message);
     */
    SocketUtils.newEventBus = function (eventHandle, eventHandler) {
        var socket = nano.socket('bus');
        var address = '/tmp/jones_core_event_' + eventHandle + '_.ipc';
        socket.connect(SocketUtils.pathToSocketAddress(address));
        socket.on('data', function (message) {
            eventHandler(eventHandle, message);
        });
        return {
            socket: socket,
            address: address
        };
    };
    /**
     *
     * @param socketType
     */
    SocketUtils.createSocket = function (socketType) {
        var socket = nano.socket(socketType);
        return socket;
    };
    /**
     *
     * @param socketType
     * @param address
     */
    SocketUtils.createSocketMaster = function (socketType, address) {
        var socket = SocketUtils.createSocket(socketType);
        SocketUtils.freeIPC(address);
        socket.bind(SocketUtils.pathToSocketAddress(address));
        return socket;
    };
    /**
     *
     * @param socketType
     * @param address
     */
    SocketUtils.createSocketSlave = function (socketType, address) {
        var socket = SocketUtils.createSocket(socketType);
        socket.connect(SocketUtils.pathToSocketAddress(address));
        return socket;
    };
    /**
     * This clears the IPC address if it is currently in use. It does this by deleting the FileSystem object that represents the IPC socket
     * @param ipcAddress
     */
    SocketUtils.freeIPC = function (ipcAddress) {
        if (fs.existsSync(ipcAddress)) {
            fs.unlinkSync(ipcAddress);
        }
    };
    /**
     * This is used to automate the sending data across a socket.  The socket connection requires a time delay, and adding that makes code ugly so this hides that. Note that this also closes the socket after, and connects on beginning
     * @param socket
     * @param request
     * @param address
     */
    SocketUtils.sendOnSocket = function (socket, request, address) {
        //console.log(address);
        socket.connect(address);
        setTimeout(function () {
            socket.send(request);
            setTimeout(function () {
                socket.shutdown(address);
            }, const_utils_1.Const.SOCKET_DELAY);
        }, const_utils_1.Const.SOCKET_DELAY);
    };
    /**
     * This clears the IPC address if it is currently in use. It does this by deleting the FileSystem object that represents the IPC socket
     * @param ipcAddress
     * @param path
     */
    SocketUtils.freeIPCfile = function (ipcAddress, path) {
        try {
            if (fs.existsSync(path + ipcAddress)) {
                fs.unlinkSync(path + ipcAddress);
            }
            else {
                if (fs.existsSync(ipcAddress)) {
                    fs.unlinkSync(ipcAddress);
                }
            }
        }
        catch (error) {
            console.log(error);
        }
    };
    /**
     *
     * @param address
     */
    SocketUtils.pathToSocketAddress = function (address) {
        //this could be extended to handle things like a file path, so TMP isnt the location anymore
        return 'ipc://' + address;
    };
    return SocketUtils;
}());
exports.SocketUtils = SocketUtils;
//# sourceMappingURL=socket.utils.js.map