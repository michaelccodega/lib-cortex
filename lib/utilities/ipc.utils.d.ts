/**
 *
 */
export declare class IPCUtils {
    static encodeGeneric(str: string): string;
    /**
     *
     * @param serviceHandle
     */
    static encodeAddressService(serviceHandle: string): string;
    /**
     *
     * @param eventHandle
     */
    static encodeAddressEvent(eventHandle: string): string;
    /**
     *
     * @param eventHandle
     */
    static encodeAddressNode(nodeHandle: string): string;
}
