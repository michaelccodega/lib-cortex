"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 *
 */
var IPCUtils = /** @class */ (function () {
    function IPCUtils() {
    }
    IPCUtils.encodeGeneric = function (str) {
        return (encodeURIComponent(str));
    };
    /**
     *
     * @param serviceHandle
     */
    IPCUtils.encodeAddressService = function (serviceHandle) {
        var address = '/tmp/jones_service_' + encodeURIComponent(serviceHandle) + '_.ipc';
        //console.log(address);
        return address;
    };
    /**
     *
     * @param eventHandle
     */
    IPCUtils.encodeAddressEvent = function (eventHandle) {
        return '/tmp/jones_core_event_' + encodeURIComponent(eventHandle) + '_.ipc';
    };
    /**
     *
     * @param eventHandle
     */
    IPCUtils.encodeAddressNode = function (nodeHandle) {
        return '/tmp/jones_core_node_' + encodeURIComponent(nodeHandle) + '_.ipc';
    };
    return IPCUtils;
}());
exports.IPCUtils = IPCUtils;
//# sourceMappingURL=ipc.utils.js.map