"use strict";
//
Object.defineProperty(exports, "__esModule", { value: true });
var Log = /** @class */ (function () {
    function Log(nodeHandle) {
        this.logs = true;
        this.nodeHandle = '';
        //
        this.nodeHandle = nodeHandle;
    }
    Log.prototype.setLogging = function (logs) {
        this.logs = logs;
    };
    Log.prototype.this = function (message) {
        if (this.logs) {
            this.log('[' + this.nodeHandle + '] ' + message + (new Date()).toDateString());
        }
    };
    Log.prototype.test = function (message) {
        if (this.logs) {
            this.log('[TEST]' + message + (new Date()).toDateString());
        }
    };
    Log.prototype.log = function (message) {
        console.log(message);
    };
    return Log;
}());
exports.Log = Log;
//# sourceMappingURL=logging.utils.js.map