import { AbstractMessageBuilder } from './abstract.message.builder';
export declare class SubscriptionMessageBuilder extends AbstractMessageBuilder {
    constructor(senderNodeHandle: string);
    setSubscriber(subscriberHandle: string): SubscriptionMessageBuilder;
    setEvent(eventHandle: string): SubscriptionMessageBuilder;
    setUnsub(): SubscriptionMessageBuilder;
    setPublish(): SubscriptionMessageBuilder;
}
