"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var abstract_message_builder_1 = require("./abstract.message.builder");
var PublishServiceMessageBuilder = /** @class */ (function (_super) {
    __extends(PublishServiceMessageBuilder, _super);
    function PublishServiceMessageBuilder(senderNodeHandle) {
        var _this = _super.call(this, senderNodeHandle) || this;
        _this.message.setType('register-service');
        _this.data['origin'] = _this.message.origin;
        return _this;
    }
    PublishServiceMessageBuilder.prototype.setServiceHandle = function (serviceHandle) {
        this.data['serviceHandle'] = serviceHandle;
        this.setData();
        return this;
    };
    PublishServiceMessageBuilder.prototype.setParentNode = function (parentNode) {
        this.data['parentNode'] = parentNode;
        this.setData();
        return this;
    };
    PublishServiceMessageBuilder.prototype.setServiceParams = function (params) {
        this.data['params'] = params;
        this.setData();
        return this;
    };
    return PublishServiceMessageBuilder;
}(abstract_message_builder_1.AbstractMessageBuilder));
exports.PublishServiceMessageBuilder = PublishServiceMessageBuilder;
//# sourceMappingURL=publish-service.message.builder.js.map