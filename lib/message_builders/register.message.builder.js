"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var abstract_message_builder_1 = require("./abstract.message.builder");
var RegisterMessageBuilder = /** @class */ (function (_super) {
    __extends(RegisterMessageBuilder, _super);
    //constructor
    function RegisterMessageBuilder(senderNodeHandle) {
        var _this = _super.call(this, senderNodeHandle) || this;
        _this.data = {};
        _this.message.setType('register')
            .setHasResponse(true);
        _this.data['nodeHandle'] = senderNodeHandle;
        _this.data['uuid'] = '';
        _this.data['parentCoreId'] = '';
        return _this;
    }
    /////////// PUBLIC BUILDER FUNCTIONS
    RegisterMessageBuilder.prototype.setUUID = function (uuid) {
        this.data['uuid'] = uuid;
        this.setData();
        return this;
    };
    RegisterMessageBuilder.prototype.setParentCoreId = function (parentCoreId) {
        this.data['parentCoreId'] = parentCoreId;
        this.setData();
        return this;
    };
    /////////// PRIVATE FUNCTIONS
    RegisterMessageBuilder.prototype.setData = function () {
        this.message.setData(this.data);
        return this;
    };
    return RegisterMessageBuilder;
}(abstract_message_builder_1.AbstractMessageBuilder));
exports.RegisterMessageBuilder = RegisterMessageBuilder;
//# sourceMappingURL=register.message.builder.js.map