import { AbstractMessageBuilder } from './abstract.message.builder';
export declare class PublishMessageBuilder extends AbstractMessageBuilder {
    constructor(senderNodeHandle: string);
    setEvent(eventHandle: string): PublishMessageBuilder;
    setMessage(message: string): PublishMessageBuilder;
}
