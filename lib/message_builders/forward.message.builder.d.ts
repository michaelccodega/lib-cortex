import { AbstractMessageBuilder } from './abstract.message.builder';
export declare class ForwardMessageBuilder extends AbstractMessageBuilder {
    setMessage(message: string): ForwardMessageBuilder;
}
