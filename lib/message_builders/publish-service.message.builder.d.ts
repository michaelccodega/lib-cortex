import { AbstractMessageBuilder } from './abstract.message.builder';
export declare class PublishServiceMessageBuilder extends AbstractMessageBuilder {
    constructor(senderNodeHandle: string);
    setServiceHandle(serviceHandle: string): PublishServiceMessageBuilder;
    setParentNode(parentNode: string): PublishServiceMessageBuilder;
    setServiceParams(params: object): PublishServiceMessageBuilder;
}
