"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var abstract_message_builder_1 = require("./abstract.message.builder");
var AckMessageBuilder = /** @class */ (function (_super) {
    __extends(AckMessageBuilder, _super);
    function AckMessageBuilder(senderNodeHandle) {
        var _this = _super.call(this, senderNodeHandle) || this;
        _this.message.setType('ack');
        _this.data = {
            type: 'ack'
        };
        return _this;
    }
    AckMessageBuilder.prototype.setAcknowledgeRegister = function () {
        this.data['type'] = 'ack_reg';
        this.setData();
        return this;
    };
    AckMessageBuilder.prototype.setAckMessage = function (ackMessage) {
        this.data['ackMessage'] = ackMessage;
        this.setData();
        return this;
    };
    return AckMessageBuilder;
}(abstract_message_builder_1.AbstractMessageBuilder));
exports.AckMessageBuilder = AckMessageBuilder;
//# sourceMappingURL=ack.message.builder.js.map