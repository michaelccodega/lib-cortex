import { AbstractMessageBuilder } from './abstract.message.builder';
export declare class GenericMessageBuilder extends AbstractMessageBuilder {
    setGenericData(data: object): GenericMessageBuilder;
}
