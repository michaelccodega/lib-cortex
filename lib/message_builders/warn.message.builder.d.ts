import { AbstractMessageBuilder } from './abstract.message.builder';
export declare class WarnMessageBuilder extends AbstractMessageBuilder {
    constructor(senderNodeHandle: string);
    warning(warning: string): WarnMessageBuilder;
}
