"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var abstract_message_builder_1 = require("./abstract.message.builder");
var GenericMessageBuilder = /** @class */ (function (_super) {
    __extends(GenericMessageBuilder, _super);
    function GenericMessageBuilder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    GenericMessageBuilder.prototype.setGenericData = function (data) {
        this.data = data;
        this.setData();
        return this;
    };
    return GenericMessageBuilder;
}(abstract_message_builder_1.AbstractMessageBuilder));
exports.GenericMessageBuilder = GenericMessageBuilder;
//# sourceMappingURL=generic.message.builder.js.map