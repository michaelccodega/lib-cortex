import { AbstractMessageBuilder } from './abstract.message.builder';
export declare class RegisterMessageBuilder extends AbstractMessageBuilder {
    protected data: object;
    constructor(senderNodeHandle: string);
    setUUID(uuid: string): RegisterMessageBuilder;
    setParentCoreId(parentCoreId: string): RegisterMessageBuilder;
    protected setData(): RegisterMessageBuilder;
}
