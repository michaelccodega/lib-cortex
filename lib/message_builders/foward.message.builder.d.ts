import { AbstractMessageBuilder } from './abstract.message.builder';
export declare class FowardMessageBuilder extends AbstractMessageBuilder {
    setMessage(message: string): FowardMessageBuilder;
}
