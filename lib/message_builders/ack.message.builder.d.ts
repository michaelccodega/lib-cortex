import { AbstractMessageBuilder } from './abstract.message.builder';
export declare class AckMessageBuilder extends AbstractMessageBuilder {
    constructor(senderNodeHandle: string);
    setAcknowledgeRegister(): AckMessageBuilder;
    setAckMessage(ackMessage: string): AckMessageBuilder;
}
