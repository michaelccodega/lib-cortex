import { Message } from '../messages';
export declare abstract class AbstractMessageBuilder {
    protected message: Message;
    protected data: object;
    protected msgGeneratorSeedId: any;
    protected messageIDgenerator: any;
    protected senderNodeHandle: string;
    constructor(senderNodeHandle: string);
    returnMessage(): Message;
    returnMessageString(): string;
    protected setData(): AbstractMessageBuilder;
}
