"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var messages_1 = require("../messages");
var random_hash_1 = require("random-hash");
var FlakeId = require("flake-idgen");
var AbstractMessageBuilder = /** @class */ (function () {
    function AbstractMessageBuilder(senderNodeHandle) {
        this.data = {};
        this.senderNodeHandle = senderNodeHandle;
        this.msgGeneratorSeedId = random_hash_1.default({
            length: 10,
            charset: [0, 1]
        });
        this.messageIDgenerator = new FlakeId({
            id: this.msgGeneratorSeedId
        });
        this.message = new messages_1.Message();
        this.message.setOrigin(senderNodeHandle)
            .setSendId(this.messageIDgenerator.next())
            .setTimestamp();
    }
    AbstractMessageBuilder.prototype.returnMessage = function () {
        this.setData();
        return this.message;
    };
    AbstractMessageBuilder.prototype.returnMessageString = function () {
        return this.message.toString();
    };
    AbstractMessageBuilder.prototype.setData = function () {
        this.message.setData(this.data);
        return this;
    };
    return AbstractMessageBuilder;
}());
exports.AbstractMessageBuilder = AbstractMessageBuilder;
//# sourceMappingURL=abstract.message.builder.js.map