"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var abstract_message_builder_1 = require("./abstract.message.builder");
var PublishMessageBuilder = /** @class */ (function (_super) {
    __extends(PublishMessageBuilder, _super);
    // Constructor
    function PublishMessageBuilder(senderNodeHandle) {
        var _this = _super.call(this, senderNodeHandle) || this;
        _this.message.setType('publish');
        return _this;
    }
    //
    PublishMessageBuilder.prototype.setEvent = function (eventHandle) {
        this.data['event'] = eventHandle;
        this.setData();
        return this;
    };
    PublishMessageBuilder.prototype.setMessage = function (message) {
        this.data['message'] = message;
        this.setData();
        return this;
    };
    return PublishMessageBuilder;
}(abstract_message_builder_1.AbstractMessageBuilder));
exports.PublishMessageBuilder = PublishMessageBuilder;
//# sourceMappingURL=publish.message.builder.js.map