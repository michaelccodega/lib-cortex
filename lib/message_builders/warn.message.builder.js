"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var abstract_message_builder_1 = require("./abstract.message.builder");
var WarnMessageBuilder = /** @class */ (function (_super) {
    __extends(WarnMessageBuilder, _super);
    function WarnMessageBuilder(senderNodeHandle) {
        var _this = _super.call(this, senderNodeHandle) || this;
        _this.message.setType('warn');
        _this.data = {
            type: 'warn',
            warning: 'unknown'
        };
        _this.setData();
        return _this;
    }
    WarnMessageBuilder.prototype.warning = function (warning) {
        this.data['warning'] = warning;
        this.setData();
        return this;
    };
    return WarnMessageBuilder;
}(abstract_message_builder_1.AbstractMessageBuilder));
exports.WarnMessageBuilder = WarnMessageBuilder;
//# sourceMappingURL=warn.message.builder.js.map