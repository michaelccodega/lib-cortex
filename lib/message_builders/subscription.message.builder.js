"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var abstract_message_builder_1 = require("./abstract.message.builder");
var SubscriptionMessageBuilder = /** @class */ (function (_super) {
    __extends(SubscriptionMessageBuilder, _super);
    function SubscriptionMessageBuilder(senderNodeHandle) {
        var _this = _super.call(this, senderNodeHandle) || this;
        _this.message.setType('subscription');
        _this.data['unsub'] = false;
        _this.data['pub'] = false;
        return _this;
    }
    SubscriptionMessageBuilder.prototype.setSubscriber = function (subscriberHandle) {
        this.data['subscriber'] = subscriberHandle;
        this.setData();
        return this;
    };
    SubscriptionMessageBuilder.prototype.setEvent = function (eventHandle) {
        this.data['eventHandle'] = eventHandle;
        this.setData();
        return this;
    };
    SubscriptionMessageBuilder.prototype.setUnsub = function () {
        this.data['unsub'] = true;
        this.setData();
        return this;
    };
    SubscriptionMessageBuilder.prototype.setPublish = function () {
        this.data['pub'] = true;
        this.setData();
        return this;
    };
    return SubscriptionMessageBuilder;
}(abstract_message_builder_1.AbstractMessageBuilder));
exports.SubscriptionMessageBuilder = SubscriptionMessageBuilder;
//# sourceMappingURL=subscription.message.builder.js.map