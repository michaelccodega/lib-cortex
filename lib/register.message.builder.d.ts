export declare class RegisterMessageBuilder {
    private message;
    private registerData;
    constructor(senderNodeHandle: string);
    setAvailableServices(services: Array<string>): RegisterMessageBuilder;
    setSubscriptions(subs: Array<string>): RegisterMessageBuilder;
    toString(): string;
    private setData();
}
