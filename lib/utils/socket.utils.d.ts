import * as nano from 'nanomsg';
export declare class SocketUtils {
    /**
     * Creates a IPC socket that listens for an event, return object with 2 params: {socket, address}
     * @param eventHandle string of the event to listen to
     * @param eventHandler takes 2 arguments: eventHandler(eventHandle, message);
     */
    static newEventBus(eventHandle: string, eventHandler: Function): object;
    static createSocket(socketType: string): nano.Socket;
    /**
     *
     * @param socketType
     * @param address
     */
    static createSocketMaster(socketType: string, address: string): nano.Socket;
    /**
     *
     * @param socketType
     * @param address
     */
    static createSocketSlave(socketType: string, address: string): nano.Socket;
    /**
     * This clears the IPC address if it is currently in use. It does this by deleting the FileSystem object that represents the IPC socket
     * @param ipcAddress
     */
    private static freeIPC(ipcAddress);
}
