"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
///
var nano = require("nanomsg");
var fs = require("fs");
var utils_1 = require("../utils");
var SocketUtils = /** @class */ (function () {
    function SocketUtils() {
    }
    /**
     * Creates a IPC socket that listens for an event, return object with 2 params: {socket, address}
     * @param eventHandle string of the event to listen to
     * @param eventHandler takes 2 arguments: eventHandler(eventHandle, message);
     */
    SocketUtils.newEventBus = function (eventHandle, eventHandler) {
        var socket = nano.socket('bus');
        var address = '/tmp/jones_core_event_' + eventHandle + '_.ipc';
        socket.connect(utils_1.Utils.pathToSocketAddress(address));
        socket.on('data', function (message) {
            eventHandler(eventHandle, message);
        });
        return {
            socket: socket,
            address: address
        };
    };
    SocketUtils.createSocket = function (socketType) {
        var socket = nano.socket(socketType);
        return socket;
    };
    /**
     *
     * @param socketType
     * @param address
     */
    SocketUtils.createSocketMaster = function (socketType, address) {
        var socket = SocketUtils.createSocket(socketType);
        SocketUtils.freeIPC(address);
        socket.bind(utils_1.Utils.pathToSocketAddress(address));
        return socket;
    };
    /**
     *
     * @param socketType
     * @param address
     */
    SocketUtils.createSocketSlave = function (socketType, address) {
        var socket = SocketUtils.createSocket(socketType);
        socket.connect(utils_1.Utils.pathToSocketAddress(address));
        return socket;
    };
    /**
     * This clears the IPC address if it is currently in use. It does this by deleting the FileSystem object that represents the IPC socket
     * @param ipcAddress
     */
    SocketUtils.freeIPC = function (ipcAddress) {
        if (fs.existsSync(ipcAddress)) {
            fs.unlinkSync(ipcAddress);
        }
    };
    return SocketUtils;
}());
exports.SocketUtils = SocketUtils;
//# sourceMappingURL=socket.utils.js.map