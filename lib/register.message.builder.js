"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var messages_1 = require("./messages");
var random_hash_1 = require("random-hash");
var FlakeId = require("flake-idgen");
var RegisterMessageBuilder = /** @class */ (function () {
    //constructor
    function RegisterMessageBuilder(senderNodeHandle) {
        this.registerData = {};
        this.message = new messages_1.default();
        var msgGeneratorSeedId = random_hash_1.default({
            length: 10,
            charset: [0, 1]
        });
        var messageIDgenerator = new FlakeId({
            id: msgGeneratorSeedId
        });
        this.message.setOrigin(senderNodeHandle)
            .setType('register')
            .setSendId('-1')
            .setSendId(messageIDgenerator.next())
            .setTimestamp();
        this.registerData['nodeHandle'] = senderNodeHandle;
        this.registerData['availableServices'] = [];
        this.registerData['subscriptions'] = [];
    }
    /////////// PUBLIC BUILDER FUNCTIONS
    RegisterMessageBuilder.prototype.setAvailableServices = function (services) {
        this.registerData['availableServices'] = services;
        this.setData();
        return this;
    };
    RegisterMessageBuilder.prototype.setSubscriptions = function (subs) {
        this.registerData['subscriptions'] = subs;
        this.setData();
        return this;
    };
    /////////// PUBLIC STRING FUNCTIONS
    RegisterMessageBuilder.prototype.toString = function () {
        //todo
        return '';
    };
    /////////// PRIVATE FUNCTIONS
    RegisterMessageBuilder.prototype.setData = function () {
        this.message.setData(this.registerData);
    };
    return RegisterMessageBuilder;
}());
exports.RegisterMessageBuilder = RegisterMessageBuilder;
//# sourceMappingURL=register.message.builder.js.map