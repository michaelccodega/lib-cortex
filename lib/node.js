"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
////////// IMPORTS
//
var utils_1 = require("./utils");
var utils_2 = require("./utils");
var utils_3 = require("./utils");
var builder_1 = require("./builder");
var builder_2 = require("./builder");
var builder_3 = require("./builder");
var publish_message_builder_1 = require("./message_builders/publish.message.builder");
// This is a self contained module that can communicate with the Cortex Core
// it is self contained, and can be used in a Cortex node as a drop in
//
var Node = /** @class */ (function () {
    //////// CONSTRUCTOR
    /**
     * Constructor for a Cortex Node
     * @param nodeHandle this is a unique string that identifies the node
     * @param config this is a JSON oject that contains parameters for construction of node
     */
    function Node(nodeHandle, config) {
        var _this = this;
        //////// FIELDS FOR THE IPC SUBSCRIPTION FUNCTIONALITY
        // This stores the list of subscriptions and sockets that listen on those IPC channels
        this.subscriptions = {};
        this.subscriptionAddresses = [];
        //////// FIELDS FOR THE IPC SERVICE FUNCTIONALITY
        // This stores a list of services that are offered by the node
        this.services = {};
        //////// FIELDS FOR DEBUGGING
        this.logging = false;
        this.test = false;
        //set up config
        this.handle = nodeHandle;
        this.configure(config);
        //rm:
        //this.logging = true;
        //set up the IPC interface
        this.ipcSocket = utils_2.SocketUtils.createSocketSlave('req', this.ipcAddress);
        this.ipcSocket.on('data', function (data) {
            _this.handleIPCdata(data.toString());
        });
        //register the node
        this.isRegistered = false;
        this.registerNode();
        this.subscribe('system', function (topic, message) {
            if (_this.logging) {
                console.log('[' + _this.handle + '] new message on topic: ' + topic);
            }
            _this.handleSystemMessage(message);
        });
        // Create a socket to listen on for the "NodeService" (implicit service with node handle as name)
        this.nodeServiceAddress = utils_3.IPCUtils.encodeAddressNode(this.handle);
        this.nodeServiceSocket = utils_2.SocketUtils.createSocketMaster('rep', this.nodeServiceAddress);
        this.nodeServiceSocket.on('data', function (msg) {
            _this.nodeServiceSocket.send(_this.handleNodeServiceMessage(msg.toJSON()));
        });
    }
    ///////////// public methods
    /**
     *
     */
    Node.prototype.spin = function () {
        // this should ensure that the thing runs... it can spawn stuff or something
        // in the future this can spawn a thread or fiber to perform maintenance or long-running processes
    };
    /**
     * Subscribe to an event, triggers callback. If callback is `undefined` it unsubscribes
     * @param eventHandle
     * @param callback
     */
    Node.prototype.subscribe = function (eventHandle, callback) {
        var _this = this;
        if (this.logging) {
            console.log('[Node] Registered Subscription: ' + eventHandle);
        }
        if (callback == undefined) {
            //unsubscribe
            this.subscriptions[eventHandle] = undefined;
            this.sendMessage((new builder_2.SubscriptionMessageBuilder(this.handle))
                .setEvent(eventHandle)
                .setSubscriber(this.handle)
                .setUnsub()
                .returnMessage());
            if (this.logging) {
                console.log('[Node] Unregistered Subscription: ' + eventHandle);
            }
            return;
        }
        else {
            this.sendMessage((new builder_2.SubscriptionMessageBuilder(this.handle))
                .setEvent(eventHandle)
                .setSubscriber(this.handle)
                .returnMessage());
            var address = utils_3.IPCUtils.encodeAddressEvent(eventHandle);
            var socket = utils_2.SocketUtils.createSocketSlave('bus', address);
            if (this.logging) {
                if (this.test) {
                    console.log('[test] listening to channel: ' + address);
                }
            }
            socket.on('data', function (data) {
                if (_this.logging) {
                    console.log('data! : ');
                }
                console.log(data.toString());
                callback(data.toString());
            });
            this.subscriptions[eventHandle] = {
                eventHandle: eventHandle,
                address: address,
                socket: socket
            };
            return;
        }
    };
    /**
     *
     * @param eventHandle
     * @param data
     */
    Node.prototype.publish = function (eventHandle, data) {
        eventHandle = utils_3.IPCUtils.encodeGeneric(eventHandle);
        if (this.logging) {
            console.log('[Node] publishing to event ' + eventHandle);
        }
        var message = new publish_message_builder_1.PublishMessageBuilder(this.handle)
            .setEvent(eventHandle)
            .setMessage(data)
            .returnMessage();
        this.sendMessage(message);
    };
    /**
     *
     * @param nodeHandle
     * @param request
     * @param callback
     */
    Node.prototype.makeRequest = function (serviceHandle, request, callback) {
        //
        /*

        NOW THIS WILL DO SOMETHING NEW ****

        This will now send the message to the core, who will send it to the correct
        service (registered service*) and use the sendMessage() function to send it to that
        node. It will go to the specific "in" channel for that node, so its known to the node
        what service it is for (maybe just the node's main address??).  The node gets the
        "origin" address and uses that to reply - and the chain cycles back.

        The node will need a "message waiting" table for messages that need a reply. Whenever
        it adds a message, it should add a timer. That timer will be for 10 seconds. After
        10 seconds, it checks the message waiting table. If that message is still there, it
        removes the entry.

        When the Node recieves a message that isnt a predefines one, it checks the message
        number with the message waiting LUT. If the messsage is in the lut, there should be
        a callback function. Else: throw out message.

        */
        var address = utils_3.IPCUtils.encodeAddressService(serviceHandle);
        //console.log(address);
        var socket = utils_2.SocketUtils.createSocketSlave('req', address);
        socket.on('data', function (response) {
            //console.log('response: ' + response);
            var res = JSON.parse(response.toString());
            callback(res);
        });
        utils_2.SocketUtils.sendOnSocket(socket, request, address);
    };
    /**
     *
     * @param serviceHandle
     * @param serviceParams
     * @param callback
     */
    Node.prototype.publishService = function (serviceHandle, serviceParams, callback) {
        if (this.logging) {
            console.log('[' + this.handle + '] CREATING NEW SERVICE: ' + serviceHandle);
        }
        var address = utils_3.IPCUtils.encodeAddressService(serviceHandle);
        var socket = utils_2.SocketUtils.createSocketMaster('rep', address);
        socket.on('data', function (data) {
            var message = JSON.parse(data.toString());
            socket.send(callback(message));
        });
        this.services[serviceHandle] = {
            serviceHandle: serviceHandle,
            serviceParams: serviceParams,
            handler: callback,
            address: address,
            socket: socket
        };
        this.sendMessage((new builder_3.PublishServiceMessageBuilder(this.handle))
            .setServiceHandle(serviceHandle)
            .setServiceParams(serviceParams)
            .returnMessage());
    };
    ///////////// PRIVATE METHODS
    // PRIVATE ACTION METHODS 
    /**
     *
     * @param msg
     */
    Node.prototype.handleIPCdata = function (msg) {
        var message = JSON.parse(msg);
        if (!message['type']) {
            if (this.logging) {
                console.log('[Node] recieved message of unknown type');
            }
        }
        else {
            switch (message['type']) {
                case 'ack': {
                    return this.handleAckMessage(message);
                }
                case 'response': {
                    return this.handleResponseMessage(message);
                }
                case 'warn': {
                    // handle this somehow???
                    return;
                }
                default: {
                    if (this.logging) {
                        console.log('[Node] recieved message of unknown type');
                    }
                }
            }
        }
    };
    // MESSAGE HANDLERS
    // ipc from core:
    /**
     *
     * @param message
     */
    Node.prototype.handleAckMessage = function (message) {
        if (message['data']) {
            if (message['data']['type'] == 'ack_reg') {
                this.isRegistered = true;
            }
        }
    };
    /**
     *
     * @param message
     */
    Node.prototype.handleResponseMessage = function (message) {
        if (this.logging) {
            console.log('got a response: ' + message);
        }
    };
    // Subscriptions:
    /**
     *
     * @param message
     */
    Node.prototype.handleSystemMessage = function (message) {
        if (this.logging) {
            console.log(message);
            //console.log(JSON.stringify(message));
        }
    };
    // Requests:
    /**
     *
     * @param message
     */
    Node.prototype.handleNodeServiceMessage = function (message) {
        if (this.logging) {
            console.log('[' + this.handle + '] new node request made...');
            console.log(JSON.stringify(message));
        }
        return '';
    };
    // UTILITY FUNCTIONS
    /**
     *
     * @param message
     */
    Node.prototype.sendMessage = function (message) {
        // this is also a god spot to save the message ID in case of callbacks?
        // make have an optional third return value that allows the callback to 
        // be saved and called on messages that use that message id
        // some sort of 'is a reply' flag?
        if (message.hasResponse) {
            //do something
        }
        if (this.logging) {
            console.log('sending the following message: ' + message.toString() + '\n');
        }
        this.ipcSocket.send(message.toString());
    };
    /**
     *
     * @param message
     */
    Node.prototype.sendString = function (message) {
        // this is also a god spot to save the message ID in case of callbacks?
        // make have an optional third return value that allows the callback to 
        // be saved and called on messages that use that message id
        // some sort of 'is a reply' flag?
        //if (message.hasResponse) {//do something}
        if (this.logging) {
            console.log(message);
        }
        this.ipcSocket.send(message.toString());
    };
    /**
     *
     */
    Node.prototype.registerNode = function () {
        var _this = this;
        // send the registration message repeatedly until it succeeds
        var services = [];
        Object.keys(this.services).forEach(function (service) {
            services.push({
                serviceHandle: service['serviceHandle'],
                origin: _this.handle,
                params: service['params']
            });
        });
        setTimeout(function () {
            _this.sendMessage((new builder_1.RegisterMessageBuilder(_this.handle))
                .returnMessage());
        }, utils_1.Const.SOCKET_DELAY);
    };
    /**
     *
     * @param config
     */
    Node.prototype.configure = function (config) {
        /////// MANIPULATE THE CONFIG OBJECT AS NEEDED 
        //todo accept a version number?
        this.config = config;
        if (this.config['ipc_addr']) {
            this.ipcAddress = this.config['ipc_addr'];
            if (this.logging) {
                console.log('[jones] Setting the IPC Address to: ' + this.ipcAddress);
            }
        }
        else {
            if (this.config['localTransport']) {
                // if want to override IPC and use TCP for some reason
                if (this.config['localTransport'] == 'tcp') {
                    this.ipcAddress = 'tcp://127.0.0.1:3000';
                }
                if (this.config['localTransport'] == 'ipc') {
                    this.ipcAddress = '/tmp/jones_core.ipc';
                }
            }
            else {
                // no default method option chosen, so go with IPC and not TCP
                this.ipcAddress = '/tmp/jones_core.ipc';
            }
        }
        if (this.config['logging']) {
            if (this.config['logging'] == true) {
                this.logging = true;
            }
        }
        if (this.config['test']) {
            if (this.config['test'] == true) {
                this.test = true;
            }
        }
    };
    return Node;
}());
exports.Node = Node;
//# sourceMappingURL=node.js.map