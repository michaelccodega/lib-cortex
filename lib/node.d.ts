import { Message } from './messages';
export declare class Node {
    handle: string;
    uuid: string;
    private config;
    private isRegistered;
    private msgGeneratorSeedId;
    private messageIDgenerator;
    private ipcAddress;
    private ipcSocket;
    private subscriptions;
    private subscriptionSocket;
    private subscriptionAddresses;
    private services;
    private nodeServiceAddress;
    private nodeServiceSocket;
    private serviceSocket;
    logging: boolean;
    test: boolean;
    /**
     * Constructor for a Cortex Node
     * @param nodeHandle this is a unique string that identifies the node
     * @param config this is a JSON oject that contains parameters for construction of node
     */
    constructor(nodeHandle: string, config: object);
    /**
     *
     */
    spin(): void;
    /**
     * Subscribe to an event, triggers callback. If callback is `undefined` it unsubscribes
     * @param eventHandle
     * @param callback
     */
    subscribe(eventHandle: string, callback: Function): void;
    /**
     *
     * @param eventHandle
     * @param data
     */
    publish(eventHandle: string, data: string): void;
    /**
     *
     * @param nodeHandle
     * @param request
     * @param callback
     */
    makeRequest(serviceHandle: string, request: string, callback: Function): void;
    /**
     *
     * @param serviceHandle
     * @param serviceParams
     * @param callback
     */
    publishService(serviceHandle: string, serviceParams: Array<string>, callback: Function): void;
    /**
     *
     * @param msg
     */
    private handleIPCdata(msg);
    /**
     *
     * @param message
     */
    private handleAckMessage(message);
    /**
     *
     * @param message
     */
    private handleResponseMessage(message);
    /**
     *
     * @param message
     */
    private handleSystemMessage(message);
    /**
     *
     * @param message
     */
    private handleNodeServiceMessage(message);
    /**
     *
     * @param message
     */
    protected sendMessage(message: Message): void;
    /**
     *
     * @param message
     */
    protected sendString(message: string): void;
    /**
     *
     */
    private registerNode();
    /**
     *
     * @param config
     */
    private configure(config);
}
