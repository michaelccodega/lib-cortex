"use strict";
/*

CREATES AND CONTROLS MESSAGES

*/
Object.defineProperty(exports, "__esModule", { value: true });
var Message = /** @class */ (function () {
    function Message() {
        this.data = {};
        //
    }
    Message.prototype.toString = function () {
        return '';
    };
    //
    Message.prototype.setType = function (type) {
        this.type = type;
        return this;
    };
    Message.prototype.setOrigin = function (origin) {
        this.origin = origin;
        return this;
    };
    Message.prototype.setData = function (data) {
        this.data = data;
        return this;
    };
    Message.prototype.setSendId = function (sendId) {
        this.sendId = sendId;
        return this;
    };
    Message.prototype.setTimestamp = function () {
        this.timestamp = (new Date()).toString();
        return this;
    };
    Message.prototype.setResponseId = function (responseId) {
        this.responseId = responseId;
        return this;
    };
    return Message;
}());
exports.Message = Message;
//# sourceMappingURL=mesages.js.map