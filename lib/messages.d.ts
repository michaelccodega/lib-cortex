export declare class Message {
    type: string;
    origin: string;
    sendId: string;
    timestamp: string;
    responseId: string;
    hasResponse: boolean;
    destination: string;
    data: object;
    constructor();
    toString(): string;
    toJSON(): object;
    setType(type: string): Message;
    setOrigin(origin: string): Message;
    setData(data: object): Message;
    setSendId(sendId: string): Message;
    setTimestamp(): Message;
    setTimestamp_(timestamp: string): Message;
    setResponseId(responseId: string): Message;
    setHasResponse(hasResponse: boolean): Message;
    setDestination(destination: string): Message;
    static Parse(str: string): Message;
}
