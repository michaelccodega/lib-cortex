export declare class Message {
    type: string;
    origin: string;
    sendId: string;
    timestamp: string;
    responseId: string;
    data: object;
    constructor();
    toString(): string;
    setType(type: string): Message;
    setOrigin(origin: string): Message;
    setData(data: object): Message;
    setSendId(sendId: string): Message;
    setTimestamp(): Message;
    setResponseId(responseId: string): Message;
}
